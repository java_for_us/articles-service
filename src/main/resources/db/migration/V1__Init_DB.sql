create sequence hibernate_sequence start 1 increment 1;
create sequence article_id_seq start 1 increment 1;
create sequence comment_id_seq start 1 increment 1;
create table article
(
    id           bigint not null,
    created_date timestamp with time zone default now(),
    updated_date timestamp with time zone default now(),
    title        varchar(255),
    text         text,
    author_id    bigint not null,
    primary key (id)
);
create table comment
(
    id           bigint not null,
    parent_id    bigint,
    created_date timestamp with time zone default now(),
    updated_date timestamp with time zone default now(),
    text         varchar(255),
    article_id   bigint not null,
    author_id    bigint not null,
    primary key (id)
);

alter table if exists comment
    add constraint comment_has_parent_article_id foreign key (article_id) references article(id) ON DELETE CASCADE;