create table article_categories
(
    article_id  bigint,
    category_id bigint,
    primary key (article_id, category_id)
);

create table categories
(
    id        bigint not null,
    parent_id bigint default null,
    name      varchar(255),
    primary key (id)
);

insert into categories(id, name)
values (1, 'Languages'),
       (2, 'GameDev');

insert into categories(parent_id, id, name)
values (1, 3, 'Java'),
       (1, 4, 'JavaScript'),
       (1, 5, 'PHP'),
       (1, 6, 'Python'),
       (2, 7, '3D sculpting');
