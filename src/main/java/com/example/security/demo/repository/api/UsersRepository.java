package com.example.security.demo.repository.api;

import com.example.security.demo.model.dto.User;
import java.util.Optional;

public interface UsersRepository {
  Optional<User> findByUsername(String username);
  Optional<User> findById(Long id);
}
