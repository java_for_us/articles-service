package com.example.security.demo.repository.api.impl;

import com.example.security.demo.model.dto.User;
import com.example.security.demo.repository.api.UsersRepository;
import com.example.security.demo.repository.api.UsersRestRepository;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class UsersRepositoryImpl implements UsersRepository {

  private final UsersRestRepository usersRestRepository;

  @Autowired
  public UsersRepositoryImpl(
      UsersRestRepository usersRestRepository) {
    this.usersRestRepository = usersRestRepository;
  }


  @Override
  public Optional<User> findByUsername(String username) {
    List<User> userList = usersRestRepository.findAll();
    return userList
        .stream()
        .filter((user) -> user.getUsername().equals(username))
        .findFirst();
  }

  @Override
  public Optional<User> findById(Long id) {
    List<User> userList = usersRestRepository.findAll();
    return userList
        .stream()
        .filter((user) -> user.getId().equals(id))
        .findFirst();
  }
}
