package com.example.security.demo.repository.postres.exectutors;

import com.example.security.demo.model.Article;
import com.example.security.demo.model.Category;
import com.example.security.demo.model.dto.ArticlesSearchPagination;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import org.springframework.data.jpa.domain.Specification;

public class ArticleExecutors implements Specification<Article> {

  private ArticlesSearchPagination articlesSearchPagination;

  public ArticleExecutors(
      ArticlesSearchPagination articlesSearchPagination) {
    this.articlesSearchPagination = articlesSearchPagination;
  }

  @Override
  public Specification<Article> and(Specification<Article> other) {
    return Specification.super.and(other);
  }

  @Override
  public Specification<Article> or(Specification<Article> other) {
    return Specification.super.or(other);
  }

  @Override
  public Predicate toPredicate(Root<Article> root, CriteriaQuery<?> criteriaQuery,
      CriteriaBuilder criteriaBuilder) {
    List<Predicate> predicates = new ArrayList<>();

    String title = articlesSearchPagination.getTitle();
    if(title != null && !title.isEmpty()) {
      predicates.add(criteriaBuilder.like(root.get("title"), "%" + articlesSearchPagination.getTitle() + "%"));
    }

    String text = articlesSearchPagination.getText();
    if(text !=null && !text.isEmpty()) {
      predicates.add(criteriaBuilder.like(root.get("text"), "%" + articlesSearchPagination.getText() + "%"));
    }

    Long authorId = articlesSearchPagination.getAuthorId();
    if(authorId !=null) {
      predicates.add(criteriaBuilder.equal(root.get("authorId"), authorId));
    }

    Set<Long> categoriesIds = articlesSearchPagination.getCategoriesIds();
    if(categoriesIds != null && !categoriesIds.isEmpty()) {
      Join<Article, Category> articleCategoryJoin = root.join("categories");
      predicates.add(articleCategoryJoin.get("id").in(categoriesIds));
    }


    return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
  }
}
