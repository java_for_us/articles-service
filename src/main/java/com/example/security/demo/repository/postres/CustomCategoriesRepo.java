package com.example.security.demo.repository.postres;

import java.util.Set;

public interface CustomCategoriesRepo {

    Boolean existsCategoriesByIds(Set<Long> ids);
}
