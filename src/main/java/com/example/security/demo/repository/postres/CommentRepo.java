package com.example.security.demo.repository.postres;

import com.example.security.demo.model.Comment;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface CommentRepo extends JpaRepository<Comment, Long> {

    // https://docs.spring.io/spring-data/jpa/docs/current/reference/html/#repositories.query-methods.query-creation
    // spring data repository
    @Query("select c from Comment c where c.articleId = :id ")
    List<Comment> findAllByArticleId(@Param("id") Long id);

    void deleteById(Long id);
}
