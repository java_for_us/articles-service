package com.example.security.demo.repository.api;

import com.example.security.demo.model.dto.User;
import java.util.List;

public interface UsersRestRepository {
  List<User> findAll();
}
