package com.example.security.demo.repository.api.impl;

import com.example.security.demo.model.dto.User;
import com.example.security.demo.repository.api.UsersRestRepository;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
@CacheConfig(cacheNames = "users.all")
public class UsersRestRepositoryImpl implements UsersRestRepository {


  private static final Logger logger = LoggerFactory.getLogger(UsersRestRepositoryImpl.class);

  private final RestTemplate restTemplate = new RestTemplate();

  private final ObjectMapper objectMapper = new ObjectMapper();

  @Value("${user.service.api}")
  private String ResourceUrl;

  @Override
  @Cacheable
  public List<User> findAll() {
    ResponseEntity<String> responseEntity = restTemplate
        .getForEntity(ResourceUrl, String.class);
    List<User> users = new ArrayList<>();

    try {
      users = objectMapper.readValue(
          responseEntity.getBody(),
          new TypeReference<List<User>>() {
          });
    } catch (JsonProcessingException error) {
      logger.error("Can't parse external json from users api", error);
    }

    return users;
  }
}
