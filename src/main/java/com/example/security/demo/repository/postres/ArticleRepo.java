package com.example.security.demo.repository.postres;

import com.example.security.demo.model.Article;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface ArticleRepo extends JpaRepository<Article, Long>,
    JpaSpecificationExecutor<Article> {

  Boolean existsArticleById(Long id);

  Boolean existsArticleByIdAndAuthorId(Long id, Long AuthorId);

  Optional<Article> findArticleById(Long id);

  @Override
  Page<Article> findAll(Specification<Article> specification, Pageable pageable);
}