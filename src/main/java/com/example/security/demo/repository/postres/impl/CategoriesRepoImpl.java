package com.example.security.demo.repository.postres.impl;

import com.example.security.demo.repository.postres.CustomCategoriesRepo;
import java.util.List;
import java.util.Set;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.stereotype.Repository;

// try EntityManager without spring data
@Repository
public class CategoriesRepoImpl implements CustomCategoriesRepo {

  @PersistenceContext
  private EntityManager em;

  @Override
  public Boolean existsCategoriesByIds(Set<Long> ids) {
    if (ids == null || ids.isEmpty()) {
      return false;
    }
    List<Long> categories =
        this.em.createQuery("SELECT a.id FROM Category a", Long.class).getResultList();
    Boolean isExist = categories.containsAll(ids);;
    return isExist;
  }
}
