package com.example.security.demo.repository.postres;

import com.example.security.demo.model.Category;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CategoriesRepo extends JpaRepository<Category, Long>, CustomCategoriesRepo {

    List<Category> findAll();
}
