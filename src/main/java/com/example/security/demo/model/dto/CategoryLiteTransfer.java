package com.example.security.demo.model.dto;

import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@AllArgsConstructor
public class CategoryLiteTransfer implements Serializable {
  private Long id;
  private Long parentId;
  private String name;
}
