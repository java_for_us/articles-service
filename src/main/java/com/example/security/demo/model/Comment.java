package com.example.security.demo.model;

import java.time.ZonedDateTime;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.Hibernate;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.UpdateTimestamp;

@Entity
@Table
@Getter
@Setter
@ToString
@RequiredArgsConstructor
@Builder
@AllArgsConstructor
public class Comment {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ID_Generator_Comment")
    @GenericGenerator(
        name = "ID_Generator_Comment",
        strategy = "enhanced-sequence",
        parameters = {
            @Parameter(name = "sequence_name", value = "comment_id_seq")
        }
    )
    private Long id;

    private Long parentId;

    @NotBlank
    private String text;

    @Column(updatable = false, name = "created_date")
    @CreationTimestamp
    private ZonedDateTime createdDate;

    @Column(name = "updated_date")
    @UpdateTimestamp
    private ZonedDateTime updatedDate;

    @NotNull
    private Long authorId;

    @NotNull
    private Long articleId;

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) {
            return false;
        }
        Comment comment = (Comment) o;
        return id != null && Objects.equals(id, comment.id);
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }
}
