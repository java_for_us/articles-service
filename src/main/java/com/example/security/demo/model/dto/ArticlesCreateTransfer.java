package com.example.security.demo.model.dto;

import com.example.security.demo.validation.ValidCategoryIds;
import java.io.Serializable;
import java.util.Set;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@AllArgsConstructor
public class ArticlesCreateTransfer implements Serializable {
  private Long id;
  private String title;
  private String text;
  private UserTransfer author;
  @ValidCategoryIds
  private Set<Long> categoriesIds;
}
