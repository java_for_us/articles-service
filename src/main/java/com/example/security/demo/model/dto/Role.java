package com.example.security.demo.model.dto;

import java.io.Serializable;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@NoArgsConstructor
@AllArgsConstructor
@Data
public class Role implements Serializable {
    private Long id;

    @Enumerated(EnumType.STRING)
    private RoleName name;

}