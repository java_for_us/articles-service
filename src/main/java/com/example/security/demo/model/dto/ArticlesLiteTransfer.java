package com.example.security.demo.model.dto;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Set;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@AllArgsConstructor
public class ArticlesLiteTransfer implements Serializable {
    private Long id;
    private String title;
    private String text;
    private UserTransfer author;
    private ZonedDateTime createdDate;
    private ZonedDateTime updatedDate;
    private Set<CategoryLiteTransfer> categories;
}
