package com.example.security.demo.model.dto;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class UserTransfer implements Serializable {
    private Long id;
    private String name;
    private String username;
    private String email;

    private Set<Role> roles = new HashSet<>();
}
