package com.example.security.demo.model.dto;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.Set;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@AllArgsConstructor
public class ArticlesWithCommentsTransfer implements Serializable {
    private Long id;
    private String title;
    private String text;
    private UserTransfer author;
    private ZonedDateTime createdDate;
    private ZonedDateTime updatedDate;
    private List<CommentTransfer> commentList;
    private Set<CategoryLiteTransfer> categories;
}
