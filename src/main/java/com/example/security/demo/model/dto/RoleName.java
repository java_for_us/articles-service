package com.example.security.demo.model.dto;

public enum RoleName {
    ROLE_USER,
    ROLE_MODERATOR,
    ROLE_ADMIN
}