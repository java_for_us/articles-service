package com.example.security.demo.model.dto;

import java.util.Set;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
@ToString
public class ArticlesSearchPagination {
      private Integer page = 1;
      private Integer limit = 10;
      private String title;
      private String text;
      private Long authorId;
      private Set<Long> categoriesIds;
}
