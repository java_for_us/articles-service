package com.example.security.demo.model.dto;

import com.example.security.demo.validation.ValidArticleId;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@AllArgsConstructor
public class CommentCreateRequest {
    private Long id;

    private Long parentId;

    private String text;

    @ValidArticleId
    private String articleId;

    private Long authorId;
}
