package com.example.security.demo.validation;

import com.example.security.demo.repository.postres.CategoriesRepo;
import java.util.Set;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import org.springframework.beans.factory.annotation.Autowired;

public class ValidCategoryIdsValidator implements ConstraintValidator<ValidCategoryIds, Set<Long>> {

  final CategoriesRepo categoriesRepo;

  @Autowired
  public ValidCategoryIdsValidator(CategoriesRepo categoriesRepo) {
    this.categoriesRepo = categoriesRepo;
  }


  @Override
  public void initialize(ValidCategoryIds constraintAnnotation) {
    //ConstraintValidator.super.initialize(constraintAnnotation);
  }

  @Override
  public boolean isValid(Set<Long> ids, ConstraintValidatorContext constraintValidatorContext) {
    return categoriesRepo.existsCategoriesByIds(ids);
  }
}
