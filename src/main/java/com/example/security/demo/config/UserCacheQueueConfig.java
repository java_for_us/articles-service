package com.example.security.demo.config;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.ExchangeBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.QueueBuilder;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class UserCacheQueueConfig {
  public static final String USER_CACHE_QUEUE = "user-clean-cache-queue";
  public static final String EXCHANGE_USER_CACHE_QUEUE = "user-clean-cache-queue-exchange";

  private static final Integer TTL_TIME = 15000;
  private static final String QUEUE_DEAD_USER_CACHE_QUEUE = "user-clean-cache-queue-dead";

  @Bean
  Queue usersCleanCacheMessagesQueue() {
    return QueueBuilder
        .durable(USER_CACHE_QUEUE)
        .withArgument("x-dead-letter-exchange", "")
        .withArgument("x-dead-letter-routing-key", QUEUE_DEAD_USER_CACHE_QUEUE)
        .withArgument("x-message-ttl", TTL_TIME)
        .build();
  }

  @Bean
  Queue deadLetterUsersCleanCacheMessagesQueue() {
    return QueueBuilder.durable(QUEUE_DEAD_USER_CACHE_QUEUE).build();
  }
  @Bean
  TopicExchange usersCleanCacheMessagesExchange() {
    return ExchangeBuilder.topicExchange(EXCHANGE_USER_CACHE_QUEUE).build();
  }

  @Bean
  Binding usersCleanCacheMessagesBinding(Queue usersCleanCacheMessagesQueue, TopicExchange usersCleanCacheMessagesExchange) {
    return BindingBuilder
        .bind(usersCleanCacheMessagesQueue)
        .to(usersCleanCacheMessagesExchange)
        .with(USER_CACHE_QUEUE);
  }
}
