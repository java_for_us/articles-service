package com.example.security.demo.controller;

import com.example.security.demo.model.dto.ArticlesCreateTransfer;
import com.example.security.demo.model.dto.ArticlesLiteTransfer;
import com.example.security.demo.model.dto.ArticlesSearchPagination;
import com.example.security.demo.service.ArticlesService;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ArticlesController {

    private final ArticlesService articlesService;

    @Autowired
    ArticlesController(ArticlesService articlesService) {
        this.articlesService = articlesService;
    }

    @GetMapping(value = "/api/articles")
    public @ResponseBody
    ResponseEntity<Page<ArticlesLiteTransfer>> get(@ModelAttribute ArticlesSearchPagination params) {
        return ResponseEntity.ok(articlesService.getArticlesWithPagination(params));
    }

    @GetMapping("/api/articles/{id}")
    public ArticlesLiteTransfer getOne(@PathVariable("id") String articleId) {
        return articlesService.getArticleById(articleId);
    }

    @PostMapping("/api/articles")
    @ResponseBody
    public ArticlesLiteTransfer create(
        @Valid @RequestBody ArticlesCreateTransfer article,
        Authentication authentication
    ) {
        return articlesService.create(article, authentication);
    }

    @PatchMapping("/api/articles/{id}")
    @ResponseBody
    public ArticlesLiteTransfer update(
        @Valid @RequestBody ArticlesCreateTransfer article,
        Authentication authentication) {
        return articlesService.update(article, authentication);
    }

    @DeleteMapping("/api/articles/{id}")
    public void delete(@PathVariable("id") String articleId,
        Authentication authentication) {
        articlesService.delete(articleId, authentication);
    }
}
