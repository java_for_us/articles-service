package com.example.security.demo.controller;

import com.example.security.demo.model.dto.CategoryLiteTransfer;
import com.example.security.demo.service.CategoriesService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CategoriesController {

  private final CategoriesService categoriesService;

  @Autowired
  public CategoriesController(CategoriesService categoriesService) {
    this.categoriesService = categoriesService;
  }

  @GetMapping(value = "/api/categories")
  public @ResponseBody
  ResponseEntity<List<CategoryLiteTransfer>> get() {
    return ResponseEntity.ok(categoriesService.getAll());
  }

}
