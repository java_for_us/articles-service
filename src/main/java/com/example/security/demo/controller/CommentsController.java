package com.example.security.demo.controller;

import com.example.security.demo.model.dto.CommentCreateRequest;
import com.example.security.demo.model.dto.CommentTransfer;
import com.example.security.demo.service.CommentsService;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CommentsController {

    private final CommentsService commentsService;

    @Autowired
    CommentsController(CommentsService commentsService) {
        this.commentsService = commentsService;
    }

    @PostMapping("/api/comments")
    @ResponseBody
    public CommentTransfer create(
        @Valid @RequestBody CommentCreateRequest comment,
        Authentication authentication) {
        return commentsService.create(comment, authentication);
    }

    @DeleteMapping("/api/comments/{id}")
    public void delete(@PathVariable Long id) {
        commentsService.delete(id);
    }


}
