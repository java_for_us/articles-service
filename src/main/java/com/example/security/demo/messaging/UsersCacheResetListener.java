package com.example.security.demo.messaging;

import com.example.security.demo.config.UserCacheQueueConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.stereotype.Component;

@Component
public class UsersCacheResetListener {

  static final Logger logger = LoggerFactory.getLogger(UsersCacheResetListener.class);

  @RabbitListener(queues = UserCacheQueueConfig.USER_CACHE_QUEUE)
  @CacheEvict(cacheNames = "users.all")
  public void process(){
    logger.info(UserCacheQueueConfig.USER_CACHE_QUEUE);
  }
}
