package com.example.security.demo.service.impl;

import static com.example.security.demo.config.CommonConfiguration.APP_CONVERSION_SERVICE;

import com.example.security.demo.model.Article;
import com.example.security.demo.model.Category;
import com.example.security.demo.model.Comment;
import com.example.security.demo.model.dto.ArticlesCreateTransfer;
import com.example.security.demo.model.dto.ArticlesLiteTransfer;
import com.example.security.demo.model.dto.ArticlesSearchPagination;
import com.example.security.demo.model.dto.ArticlesWithCommentsTransfer;
import com.example.security.demo.model.dto.CategoryLiteTransfer;
import com.example.security.demo.model.dto.CommentTransfer;
import com.example.security.demo.model.dto.RoleName;
import com.example.security.demo.model.dto.User;
import com.example.security.demo.model.dto.UserTransfer;
import com.example.security.demo.repository.api.UsersRepository;
import com.example.security.demo.repository.postres.ArticleRepo;
import com.example.security.demo.repository.postres.CategoriesRepo;
import com.example.security.demo.repository.postres.CommentRepo;
import com.example.security.demo.repository.postres.exectutors.ArticleExecutors;
import com.example.security.demo.security.UserPrinciple;
import com.example.security.demo.service.ArticlesService;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.convert.ConversionService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ResponseStatusException;

@Component
public class ArticlesServiceImpl implements ArticlesService {


    private final ConversionService conversionService;

    private final ArticleRepo articleRepo;

    private final UsersRepository usersRepository;

    private final CategoriesRepo categoriesRepo;

    private final CommentRepo commentRepo;

    @Autowired
    public ArticlesServiceImpl(
        @Qualifier(APP_CONVERSION_SERVICE) ConversionService conversionService,
        ArticleRepo articleRepo,
        UsersRepository usersRepository,
        CategoriesRepo categoriesRepo,
        CommentRepo commentRepo) {
        this.conversionService = conversionService;
        this.articleRepo = articleRepo;
        this.categoriesRepo = categoriesRepo;
        this.usersRepository = usersRepository;
        this.commentRepo = commentRepo;
    }

    @Override
    public Page<ArticlesLiteTransfer> getArticlesWithPagination(
        ArticlesSearchPagination articlesSearchPagination) {
        Integer page = articlesSearchPagination.getPage();
        Integer limit = articlesSearchPagination.getLimit();
        Pageable currentRequest = PageRequest.of(page - 1, limit);
        Page<Article> articleList = articleRepo.findAll(
            new ArticleExecutors(articlesSearchPagination), currentRequest);

        return articleList.map(this::mapArticleToArticleWithUser);
    }

    @Override
    public ArticlesLiteTransfer getArticleById(String id) {
        Optional<Article> optionalArticle = articleRepo.findById(Long.parseLong(id));
        //List<Comment> commentList = commentRepo.findAllByArticleId(Long.parseLong(id));
        if (optionalArticle.isPresent()) {
            Article article = optionalArticle.get();
            return mapArticleToArticleWithUser(article);
        }
        return ArticlesLiteTransfer.builder().title("Not Find").build();
    }

    @Override
    public ArticlesWithCommentsTransfer getFullArticleById(String id) {
        Optional<Article> optionalArticle = articleRepo.findById(Long.parseLong(id));
        if (optionalArticle.isPresent()) {
            // TODO replace to converter
            Article article = optionalArticle.get();
            List<Comment> commentList = commentRepo.findAllByArticleId(article.getId());
            ;
            List<CommentTransfer> commentTransferList = commentList.stream().map((comment) -> {
                Optional<User> userOptional = usersRepository.findById(comment.getAuthorId());
                User user = userOptional.get();
                UserTransfer userTransfer = conversionService.convert(user, UserTransfer.class);
                CommentTransfer commentTransfer = conversionService.convert(comment,
                    CommentTransfer.class);
                commentTransfer.setAuthor(userTransfer);
                return commentTransfer;
            }).collect(Collectors.toList());

            ArticlesWithCommentsTransfer articlesWithComments = conversionService.convert(article,
                ArticlesWithCommentsTransfer.class);
            Optional<User> userOptional = usersRepository.findById(article.getAuthorId());
            User user = userOptional.get();
            UserTransfer userTransfer = conversionService.convert(user, UserTransfer.class);
            articlesWithComments.setAuthor(userTransfer);
            articlesWithComments.setCommentList(commentTransferList);
            return articlesWithComments;
        }


        return ArticlesWithCommentsTransfer.builder().build();
    }

    @Override
    public ArticlesLiteTransfer create(ArticlesCreateTransfer articlesToCreate, Authentication authentication) {
        UserTransfer user = getUserTransferFromAuthentication(authentication);
        articlesToCreate.setAuthor(user);

        Article articleToSave = conversionService.convert(articlesToCreate, Article.class);
        Set<Category> categorySet = getCategoriesFromCategoriesIds(articlesToCreate.getCategoriesIds());
        articleToSave.setCategories(categorySet);
        //articleToSave.setCreatedDate(ZonedDateTime.now());

        Article article = articleRepo.save(articleToSave);

        return conversionService.convert(article, ArticlesLiteTransfer.class);
    }


    @Override
    public ArticlesLiteTransfer update(ArticlesCreateTransfer articleToUpdate,
        Authentication authentication)
        throws ResponseStatusException {
        if (isEditable(articleToUpdate.getId(), authentication)) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN);
        }

        Optional<Article> optionalArticleFromDb = articleRepo.findArticleById(
            articleToUpdate.getId());

        if (optionalArticleFromDb.isEmpty()) {
            return create(articleToUpdate, authentication);
        }
        UserTransfer user = getUserTransferFromAuthentication(authentication);
        articleToUpdate.setAuthor(user);

        Article articleFromDb = optionalArticleFromDb.get();
        BeanUtils.copyProperties(articleToUpdate, articleFromDb, "id");
        Set<Category> categorySet = getCategoriesFromCategoriesIds(
            articleToUpdate.getCategoriesIds());
        Article articleToSave = conversionService.convert(articleToUpdate, Article.class);
        articleToSave.setCategories(categorySet);
        Article updatedArticle = articleRepo.save(articleToSave);

        return conversionService.convert(updatedArticle, ArticlesLiteTransfer.class);
    }

    @Override
    public void delete(String id, Authentication authentication) {
        if (isEditable(Long.parseLong(id), authentication)) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN);
        }
        Article article = new Article();
        article.setId(Long.parseLong(id));
        articleRepo.delete(article);
    }

    private ArticlesLiteTransfer mapArticleToArticleWithUser(Article article) {
        ArticlesLiteTransfer articlesLiteTransfer = conversionService.convert(article,
            ArticlesLiteTransfer.class);
        Optional<User> userOptional = usersRepository.findById(article.getAuthorId());
        if (userOptional.isPresent()) {
            User user = userOptional.get();
            UserTransfer userTransfer = conversionService.convert(user, UserTransfer.class);
            articlesLiteTransfer.setAuthor(userTransfer);
        }

        return articlesLiteTransfer;
    }

    private Map<Long, CategoryLiteTransfer> getMapCategories() {
        List<Category> categoryList = categoriesRepo.findAll();

        return categoryList.stream()
            .collect(Collectors.toMap(
                Category::getId,
                (category) -> conversionService.convert(category, CategoryLiteTransfer.class))
            );
    }

    private Set<Category> getCategoriesFromCategoriesIds(Set<Long> categoriesIds){
        Map<Long, CategoryLiteTransfer> mapCategories = getMapCategories();
        return categoriesIds
            .stream()
            .filter((id) -> mapCategories.get(id) != null)
            .map((id) -> conversionService.convert(mapCategories.get(id), Category.class))
            .collect(Collectors.toSet());
    }

    private UserTransfer getUserTransferFromAuthentication(Authentication authentication) {
        UserPrinciple userDetails = (UserPrinciple) authentication.getPrincipal();
        return UserTransfer.builder().id(userDetails.getId()).build();
    }

    private boolean isEditable(Long id, Authentication authentication) {
        UserPrinciple userDetails = (UserPrinciple) authentication.getPrincipal();
        boolean isAdmin = userDetails.hasAuthority(RoleName.ROLE_ADMIN.name());
        Boolean isUserAuthorThisArticle =
            articleRepo.existsArticleByIdAndAuthorId(id, userDetails.getId());
        return !isAdmin && !isUserAuthorThisArticle;
    }

}
