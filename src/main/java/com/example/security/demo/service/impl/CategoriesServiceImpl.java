package com.example.security.demo.service.impl;

import static com.example.security.demo.config.CommonConfiguration.APP_CONVERSION_SERVICE;

import com.example.security.demo.model.Category;
import com.example.security.demo.model.dto.CategoryLiteTransfer;
import com.example.security.demo.repository.postres.CategoriesRepo;
import com.example.security.demo.service.CategoriesService;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Component;

@Component
public class CategoriesServiceImpl implements CategoriesService {

  private final ConversionService conversionService;

  private final CategoriesRepo categoriesRepo;

  @Autowired
  public CategoriesServiceImpl(
      @Qualifier(APP_CONVERSION_SERVICE)  ConversionService conversionService,
      CategoriesRepo categoriesRepo) {
      this.conversionService = conversionService;
      this.categoriesRepo = categoriesRepo;
  }

  @Override
  public List<CategoryLiteTransfer> getAll() {
    List<Category> list = categoriesRepo.findAll();

    return list.stream()
        .map((category)-> conversionService.convert(category, CategoryLiteTransfer.class))
        .collect(Collectors.toList());
  }
}
