package com.example.security.demo.service.impl;

import static com.example.security.demo.config.CommonConfiguration.APP_CONVERSION_SERVICE;

import com.example.security.demo.model.Comment;
import com.example.security.demo.model.dto.CommentCreateRequest;
import com.example.security.demo.model.dto.CommentTransfer;
import com.example.security.demo.repository.postres.CommentRepo;
import com.example.security.demo.security.UserPrinciple;
import com.example.security.demo.service.CommentsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.convert.ConversionService;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

@Component
public class CommentServiceImpl implements CommentsService {

    private final ConversionService conversionService;

    private final CommentRepo commentRepo;

    @Autowired
    CommentServiceImpl(
        @Qualifier(APP_CONVERSION_SERVICE) ConversionService conversionService,
        CommentRepo commentRepo
    ) {
        this.conversionService = conversionService;
        this.commentRepo = commentRepo;
    }

    @Override
    public CommentTransfer create(
            CommentCreateRequest commentTransfer,
            Authentication authentication) {
        UserPrinciple userDetails = (UserPrinciple) authentication.getPrincipal();
        commentTransfer.setAuthorId(userDetails.getId());
        Comment comment = conversionService.convert(commentTransfer, Comment.class);
        commentRepo.save(comment);
        return conversionService.convert(comment, CommentTransfer.class);
    }

    @Override
    public void delete(Long commentId) {
        commentRepo.deleteById(commentId);
    }
}
