package com.example.security.demo.service;

import com.example.security.demo.model.dto.ArticlesCreateTransfer;
import com.example.security.demo.model.dto.ArticlesLiteTransfer;
import com.example.security.demo.model.dto.ArticlesSearchPagination;
import com.example.security.demo.model.dto.ArticlesWithCommentsTransfer;
import org.springframework.data.domain.Page;
import org.springframework.security.core.Authentication;
import org.springframework.web.server.ResponseStatusException;

public interface ArticlesService {

    Page<ArticlesLiteTransfer> getArticlesWithPagination(
        ArticlesSearchPagination articlesSearchPagination);

    ArticlesLiteTransfer getArticleById(String id);

    ArticlesWithCommentsTransfer getFullArticleById(String id);

    ArticlesLiteTransfer create(ArticlesCreateTransfer article, Authentication authentication);

    ArticlesLiteTransfer update(ArticlesCreateTransfer article, Authentication authentication)
        throws ResponseStatusException;

    void delete(String id, Authentication authentication);
}
