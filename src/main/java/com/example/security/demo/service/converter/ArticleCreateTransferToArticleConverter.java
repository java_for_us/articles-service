package com.example.security.demo.service.converter;

import com.example.security.demo.model.Article;
import com.example.security.demo.model.dto.ArticlesCreateTransfer;
import java.time.ZonedDateTime;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class ArticleCreateTransferToArticleConverter implements Converter<ArticlesCreateTransfer, Article> {

  @Override
  public Article convert(ArticlesCreateTransfer source) {

    return Article.builder()
        .id(source.getId())
        .title(source.getTitle())
        .text(source.getText())
        .authorId(source.getAuthor().getId())
        .updatedDate(ZonedDateTime.now())
        .build();
  }

}
