package com.example.security.demo.service.converter;

import com.example.security.demo.model.Category;
import com.example.security.demo.model.dto.CategoryLiteTransfer;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class CategoryLiteTransferToCategoryConverter implements Converter<CategoryLiteTransfer, Category> {

  @Override
  public Category convert(CategoryLiteTransfer source) {
    return Category.builder()
        .id(source.getId())
        .name(source.getName())
        .parentId(source.getParentId())
        .build();
  }
}
