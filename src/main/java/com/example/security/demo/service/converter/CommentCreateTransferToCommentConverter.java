package com.example.security.demo.service.converter;

import com.example.security.demo.model.Comment;
import com.example.security.demo.model.dto.CommentCreateRequest;
import java.time.ZonedDateTime;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class CommentCreateTransferToCommentConverter implements Converter<CommentCreateRequest, Comment> {

    @Override
    public Comment convert(CommentCreateRequest source) {
        return Comment.builder()
            .id(source.getId())
            .parentId(source.getParentId())
            .text(source.getText())
            .createdDate(ZonedDateTime.now())
            .updatedDate(ZonedDateTime.now())
            .authorId(source.getAuthorId())
            .articleId(Long.parseLong(source.getArticleId()))
                .build();
    }
}
