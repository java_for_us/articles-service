package com.example.security.demo.service;

import com.example.security.demo.model.dto.CategoryLiteTransfer;
import java.util.List;

public interface CategoriesService {
      List<CategoryLiteTransfer> getAll();
}
