package com.example.security.demo.service.converter;

import com.example.security.demo.model.Comment;
import com.example.security.demo.model.dto.CommentTransfer;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class CommentToCommentTransferConverter implements Converter<Comment, CommentTransfer> {

    @Override
    public CommentTransfer convert(Comment source) {
        return CommentTransfer.builder()
                .id(source.getId())
                .parentId(source.getParentId())
                .text(source.getText())
                .createdDate(source.getCreatedDate())
                .updatedDate(source.getUpdatedDate())
                .build();
    }
}
