package com.example.security.demo.service.converter;

import com.example.security.demo.model.Comment;
import com.example.security.demo.model.dto.CommentTransfer;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class CommentTransferToCommentConverter implements Converter<CommentTransfer, Comment> {

    @Override
    public Comment convert(CommentTransfer source) {
        return Comment.builder()
            .id(source.getId())
            .parentId(source.getParentId())
            .text(source.getText())
            .authorId(source.getId())
            .articleId(source.getArticle().getId())
                .build();
    }
}
