package com.example.security.demo.service.converter;

import com.example.security.demo.model.Article;
import com.example.security.demo.model.Category;
import com.example.security.demo.model.dto.ArticlesLiteTransfer;
import java.util.Set;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class ArticleLiteTransferToArticleConverter implements Converter<ArticlesLiteTransfer, Article> {

    private final CategoryLiteTransferToCategoryConverter categoryLiteTransferToCategoryConverter;

    @Autowired
    public ArticleLiteTransferToArticleConverter(CategoryLiteTransferToCategoryConverter converter) {
        this.categoryLiteTransferToCategoryConverter = converter;
    }

    @Override
    public Article convert(ArticlesLiteTransfer source) {
        Set<Category> categories = source.getCategories().stream()
            .map(categoryLiteTransferToCategoryConverter::convert)
            .collect(Collectors.toSet());

        return Article.builder()
                .id(source.getId())
                .title(source.getTitle())
                .text(source.getText())
                .authorId(source.getAuthor().getId())
                .categories(categories)
                .build();
    }
}
