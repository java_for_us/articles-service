package com.example.security.demo.service.converter;

import com.example.security.demo.model.Article;
import com.example.security.demo.model.dto.ArticlesLiteTransfer;
import com.example.security.demo.model.dto.CategoryLiteTransfer;
import java.util.Set;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class ArticleToArticlesLiteTransferConverter implements Converter<Article, ArticlesLiteTransfer> {

    private final CategoryToCategoryLiteTransferConverter categoryToCategoryLiteTransferConverter;

    @Autowired
    public ArticleToArticlesLiteTransferConverter(CategoryToCategoryLiteTransferConverter converter) {
        categoryToCategoryLiteTransferConverter = converter;
    }

    @Override
    public ArticlesLiteTransfer convert(Article source) {
        Set<CategoryLiteTransfer> categories = source.getCategories().stream()
            .map(categoryToCategoryLiteTransferConverter::convert)
            .collect(Collectors.toSet());

        return ArticlesLiteTransfer.builder()
                .id(source.getId())
                .title(source.getTitle())
                .text(source.getText())
                .createdDate(source.getCreatedDate())
                .updatedDate(source.getUpdatedDate())
                .categories(categories)
                .build();
    }
}
