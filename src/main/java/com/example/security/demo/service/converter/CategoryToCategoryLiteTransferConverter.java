package com.example.security.demo.service.converter;

import com.example.security.demo.model.Category;
import com.example.security.demo.model.dto.CategoryLiteTransfer;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class CategoryToCategoryLiteTransferConverter implements Converter<Category, CategoryLiteTransfer> {

  @Override
  public CategoryLiteTransfer convert(Category source) {
    return CategoryLiteTransfer.builder()
        .id(source.getId())
        .name(source.getName())
        .parentId(source.getParentId())
        .build();
  }
}
